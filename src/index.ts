import express from 'express';
import cors from 'cors';
import * as dotenv from 'dotenv';
import { AppRouter } from './AppRouter';
import { connectDB } from './db/connect';
import './controllers/LoginController';
import './controllers/TaskController';
import { errorHandlerMiddleware } from './middlewares/errorHandler';
dotenv.config();
import cookieParser from 'cookie-parser';

const app = express();
app.use(express.json());
app.use(
  cors({
    origin: 'https://task-apps-txhai12.netlify.app',
    credentials: true,
  })
);
app.use(cookieParser());
app.use(AppRouter.getInstance());

app.use(errorHandlerMiddleware);
const port = process.env.PORT || 3000;

const start = async () => {
  try {
    await connectDB(process.env.MONGO_URI as string);
    app.listen(port, () => {
      console.log(`app is listening on port ${port}`);
    });
  } catch (err) {
    console.log(err);
  }
};

start();
