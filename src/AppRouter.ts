import express from 'express';

export class AppRouter {
  private static instanse: express.Router;
  static getInstance(): express.Router {
    if (!AppRouter.instanse) {
      AppRouter.instanse = express.Router();
    }
    return AppRouter.instanse;
  }
}
