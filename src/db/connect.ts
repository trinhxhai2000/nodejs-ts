import mongoose, { Mongoose, ConnectOptions } from 'mongoose';

export const connectDB = (url: string): Promise<Mongoose> => {
  return mongoose.connect(url, {
    useNewUrlParser: true,
    // useFindAndModify: false,
    useUnifiedTopology: true,
  } as ConnectOptions);
};
