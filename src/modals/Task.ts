import mongoose from 'mongoose';

export const taskSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Must provide name'],
    trim: true,
    maxlength: [20, 'Name can not be more than 20 characters'],
  },
  content: {
    type: String,
    required: [false, 'Must provide content'],
    trim: true,
    maxlength: [500, 'Content can not be more than 500 characters'],
  },
  completed: {
    type: Boolean,
    default: false,
  },
  author: {
    type: String,
    required: [true, 'Must provide content'],
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

export const TaskModal = mongoose.model('Task', taskSchema);
