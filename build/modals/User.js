"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModel = void 0;
var mongoose_1 = __importDefault(require("mongoose"));
var bcryptjs_1 = __importDefault(require("bcryptjs"));
var userSchema = new mongoose_1.default.Schema({
    username: {
        type: String,
        required: [true, 'username must be provided'],
        unique: true,
    },
    password: {
        type: String,
        required: [true, 'password price must be provided'],
    },
});
userSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcryptjs_1.default.genSalt(10, function (saltError, salt) {
            if (saltError) {
                return next(saltError);
            }
            else {
                bcryptjs_1.default.hash(user.password, salt, function (hashError, hash) {
                    if (hashError) {
                        return next(hashError);
                    }
                    user.password = hash;
                    next();
                });
            }
        });
    }
    else {
        return next();
    }
});
userSchema.methods.comparePassword = function (password, callback) {
    bcryptjs_1.default.compare(password, this.password, function (error, isMatch) {
        if (error) {
            return callback(error);
        }
        else {
            callback(null, isMatch);
        }
    });
};
exports.UserModel = mongoose_1.default.model('User', userSchema);
