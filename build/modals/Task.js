"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TaskModal = exports.taskSchema = void 0;
var mongoose_1 = __importDefault(require("mongoose"));
exports.taskSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: [true, 'Must provide name'],
        trim: true,
        maxlength: [20, 'Name can not be more than 20 characters'],
    },
    content: {
        type: String,
        required: [false, 'Must provide content'],
        trim: true,
        maxlength: [500, 'Content can not be more than 500 characters'],
    },
    completed: {
        type: Boolean,
        default: false,
    },
    author: {
        type: String,
        required: [true, 'Must provide content'],
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },
});
exports.TaskModal = mongoose_1.default.model('Task', exports.taskSchema);
