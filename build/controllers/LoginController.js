"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginController = void 0;
var User_1 = require("../modals/User");
var use_1 = require("./decorators/use");
var routes_1 = require("./decorators/routes");
var controller_1 = require("./decorators/controller");
var asyncWrapper_1 = require("./decorators/asyncWrapper");
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var http_status_codes_1 = require("http-status-codes");
var AppConst_1 = require("../AppConst");
var requireAuth_1 = require("../middlewares/requireAuth");
var LoginController = /** @class */ (function () {
    function LoginController() {
    }
    // add require req.body: username, password
    LoginController.prototype.login = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, username, password, user, payload_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = req.body, username = _a.username, password = _a.password;
                        console.log('login');
                        return [4 /*yield*/, User_1.UserModel.findOne({ username: username })];
                    case 1:
                        user = _b.sent();
                        console.log('user', user);
                        if (user) {
                            payload_1 = { username: user.username };
                            user.comparePassword(password, function (err, isMatch) {
                                console.log('comparePassword', isMatch);
                                if (isMatch) {
                                    var token = jsonwebtoken_1.default.sign(payload_1, process.env.JWT_SECRET, {
                                        expiresIn: '30d',
                                    });
                                    console.log('Set cookie nek:', user);
                                    res.cookie(AppConst_1.AUTH_COOKIE_KEY, token, {
                                        maxAge: 3000 * 9000,
                                        sameSite: 'none',
                                        secure: true,
                                        httpOnly: true,
                                        path: '/',
                                    });
                                    res.status(http_status_codes_1.StatusCodes.OK).json({
                                        success: true,
                                        // user: { username: user.username, user_id: user._id },
                                    });
                                }
                                else {
                                    res.status(http_status_codes_1.StatusCodes.OK).json({
                                        success: false,
                                        message: 'Invalid username or password!',
                                    });
                                    return;
                                }
                            });
                            return [2 /*return*/];
                        }
                        else {
                            res
                                .status(http_status_codes_1.StatusCodes.OK)
                                .json({ success: false, message: 'Invalid username or password!' });
                            return [2 /*return*/];
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    // add require req.body: username, password
    LoginController.prototype.register = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var username, user, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        username = req.body.username;
                        return [4 /*yield*/, User_1.UserModel.findOne({ username: username })];
                    case 1:
                        user = _a.sent();
                        if (!!user) return [3 /*break*/, 6];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, User_1.UserModel.create(req.body)];
                    case 3:
                        _a.sent();
                        res.status(http_status_codes_1.StatusCodes.CREATED).json({ success: true });
                        return [3 /*break*/, 5];
                    case 4:
                        err_1 = _a.sent();
                        res
                            .status(http_status_codes_1.StatusCodes.BAD_REQUEST)
                            .json({ success: false, message: err_1 });
                        return [3 /*break*/, 5];
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        res
                            .status(http_status_codes_1.StatusCodes.BAD_REQUEST)
                            .json({ success: false, message: "username ".concat(username, " exist !") });
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    LoginController.prototype.getLoginUser = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var username, user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        username = req.body.username;
                        console.log('getLoginUser', req.body);
                        return [4 /*yield*/, User_1.UserModel.findOne({ username: username })];
                    case 1:
                        user = _a.sent();
                        console.log('find user', user);
                        if (user) {
                            res.status(200).json({
                                success: true,
                                user: { username: user.username, user_id: user._id },
                            });
                        }
                        else {
                            res.status(200).json({ success: true, user: null });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginController.prototype.logout = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                res.clearCookie(AppConst_1.AUTH_COOKIE_KEY, {
                    sameSite: 'none',
                    secure: true,
                    httpOnly: true,
                    path: '/',
                });
                res.status(200).json({ success: 200 });
                return [2 /*return*/];
            });
        });
    };
    __decorate([
        (0, asyncWrapper_1.asyncWrapper)(true),
        (0, routes_1.post)('/login'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", Promise)
    ], LoginController.prototype, "login", null);
    __decorate([
        (0, asyncWrapper_1.asyncWrapper)(true),
        (0, routes_1.post)('/register'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", Promise)
    ], LoginController.prototype, "register", null);
    __decorate([
        (0, use_1.use)(requireAuth_1.requireAuth),
        (0, asyncWrapper_1.asyncWrapper)(true),
        (0, routes_1.get)('/getLoginUser'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", Promise)
    ], LoginController.prototype, "getLoginUser", null);
    __decorate([
        (0, use_1.use)(requireAuth_1.requireAuth),
        (0, asyncWrapper_1.asyncWrapper)(true),
        (0, routes_1.post)('/logout'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", Promise)
    ], LoginController.prototype, "logout", null);
    LoginController = __decorate([
        (0, controller_1.controller)('/api/v1')
    ], LoginController);
    return LoginController;
}());
exports.LoginController = LoginController;
