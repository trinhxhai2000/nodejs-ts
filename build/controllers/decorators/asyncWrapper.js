"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.asyncWrapper = void 0;
require("reflect-metadata");
var MetadataKey_1 = require("./MetadataKey");
function asyncWrapper(value) {
    return function (target, key, desc) {
        Reflect.defineMetadata(MetadataKey_1.MetadataKeys.asyncWrapper, true, target, key);
    };
}
exports.asyncWrapper = asyncWrapper;
