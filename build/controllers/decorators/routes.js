"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.patch = exports.del = exports.put = exports.post = exports.get = void 0;
require("reflect-metadata");
var Methods_1 = require("./Methods");
var MetadataKey_1 = require("./MetadataKey");
function RouteBinder(method) {
    return function (path) {
        return function (target, key, desc) {
            Reflect.defineMetadata(MetadataKey_1.MetadataKeys.path, path, target, key);
            Reflect.defineMetadata(MetadataKey_1.MetadataKeys.method, method, target, key);
        };
    };
}
exports.get = RouteBinder(Methods_1.Methods.get);
exports.post = RouteBinder(Methods_1.Methods.post);
exports.put = RouteBinder(Methods_1.Methods.put);
exports.del = RouteBinder(Methods_1.Methods.delete);
exports.patch = RouteBinder(Methods_1.Methods.patch);
