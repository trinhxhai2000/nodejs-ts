"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TaskController = void 0;
var routes_1 = require("./decorators/routes");
var use_1 = require("./decorators/use");
var controller_1 = require("./decorators/controller");
var requireAuth_1 = require("../middlewares/requireAuth");
var Task_1 = require("../modals/Task");
var http_status_codes_1 = require("http-status-codes");
var CustomError_1 = require("../errors/CustomError");
var TaskController = /** @class */ (function () {
    function TaskController() {
    }
    TaskController.prototype.getAllTask = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, completed, name, sort, fields, queryObject, result, sortList, fieldsList, page, limit, skip, tasks;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = req.query, completed = _a.completed, name = _a.name, sort = _a.sort, fields = _a.fields;
                        queryObject = { author: req.body.username };
                        if (completed) {
                            queryObject.completed = completed === 'true' ? true : false;
                        }
                        if (name) {
                            queryObject.name = { $regex: name, $options: 'i' };
                        }
                        result = Task_1.TaskModal.find(queryObject);
                        if (sort) {
                            sortList = sort.split(',').join(' ');
                            result = result.sort(sortList);
                        }
                        else {
                            result = result.sort('createdAt');
                        }
                        if (fields) {
                            fieldsList = fields.split(',').join(' ');
                            result = result.select(fieldsList);
                        }
                        page = Number(req.query.page) || 1;
                        limit = Number(req.query.limit) || 1000000;
                        skip = (page - 1) * limit;
                        result = result.skip(skip).limit(limit);
                        return [4 /*yield*/, result];
                    case 1:
                        tasks = _b.sent();
                        res.status(http_status_codes_1.StatusCodes.OK).json({ tasks: tasks, nbHits: tasks.length });
                        return [2 /*return*/];
                }
            });
        });
    };
    TaskController.prototype.getTask = function (req, res, next) {
        return __awaiter(this, void 0, void 0, function () {
            var taskID, task;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        taskID = req.params.id;
                        return [4 /*yield*/, Task_1.TaskModal.findOne({
                                _id: taskID,
                                author: req.body.username,
                            })];
                    case 1:
                        task = _a.sent();
                        if (!task) {
                            return [2 /*return*/, next((0, CustomError_1.createCustomError)("No task with id : ".concat(taskID), http_status_codes_1.StatusCodes.NOT_FOUND))];
                        }
                        res.status(http_status_codes_1.StatusCodes.OK).json({ success: true, data: task });
                        return [2 /*return*/];
                }
            });
        });
    };
    //add bodyvalidator
    TaskController.prototype.createTask = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var newTask, task;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        newTask = __assign(__assign({}, req.body), { author: req.body.username });
                        return [4 /*yield*/, Task_1.TaskModal.create(newTask)];
                    case 1:
                        task = _a.sent();
                        res.status(http_status_codes_1.StatusCodes.CREATED).json({ success: true, task: task });
                        return [2 /*return*/];
                }
            });
        });
    };
    TaskController.prototype.deleteTask = function (req, res, next) {
        return __awaiter(this, void 0, void 0, function () {
            var taskID, task;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        taskID = req.params.id;
                        return [4 /*yield*/, Task_1.TaskModal.findOneAndDelete({
                                _id: taskID,
                                author: req.body.username,
                            })];
                    case 1:
                        task = _a.sent();
                        if (!task) {
                            return [2 /*return*/, next((0, CustomError_1.createCustomError)("No task with id : ".concat(taskID), http_status_codes_1.StatusCodes.NOT_FOUND))];
                        }
                        res.status(http_status_codes_1.StatusCodes.OK).json({ success: true });
                        return [2 /*return*/];
                }
            });
        });
    };
    TaskController.prototype.updateTask = function (req, res, next) {
        return __awaiter(this, void 0, void 0, function () {
            var taskID, task;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        taskID = req.params.id;
                        return [4 /*yield*/, Task_1.TaskModal.findOneAndUpdate({ _id: taskID, author: req.body.username }, req.body, {
                                new: true,
                                runValidators: true,
                            })];
                    case 1:
                        task = _a.sent();
                        if (!task) {
                            return [2 /*return*/, next((0, CustomError_1.createCustomError)("No task with id : ".concat(taskID), http_status_codes_1.StatusCodes.NOT_FOUND))];
                        }
                        res.status(http_status_codes_1.StatusCodes.OK).json({ task: task });
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        (0, use_1.use)(requireAuth_1.requireAuth),
        (0, routes_1.get)('/'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", Promise)
    ], TaskController.prototype, "getAllTask", null);
    __decorate([
        (0, use_1.use)(requireAuth_1.requireAuth),
        (0, routes_1.get)('/:id'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object, Function]),
        __metadata("design:returntype", Promise)
    ], TaskController.prototype, "getTask", null);
    __decorate([
        (0, use_1.use)(requireAuth_1.requireAuth),
        (0, routes_1.post)('/'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object]),
        __metadata("design:returntype", Promise)
    ], TaskController.prototype, "createTask", null);
    __decorate([
        (0, use_1.use)(requireAuth_1.requireAuth),
        (0, routes_1.del)('/:id'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object, Function]),
        __metadata("design:returntype", Promise)
    ], TaskController.prototype, "deleteTask", null);
    __decorate([
        (0, use_1.use)(requireAuth_1.requireAuth),
        (0, routes_1.patch)('/:id'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, Object, Function]),
        __metadata("design:returntype", Promise)
    ], TaskController.prototype, "updateTask", null);
    TaskController = __decorate([
        (0, controller_1.controller)('/api/v1/tasks')
    ], TaskController);
    return TaskController;
}());
exports.TaskController = TaskController;
